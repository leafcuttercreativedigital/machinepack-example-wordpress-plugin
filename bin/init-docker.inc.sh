if test ! -d /var/www/html
then
	echo 'Please only run this inside a container'
	exit 1
fi

if test ! -x /usr/local/bin/wp
then
	echo "Downloading WPCLI"
	curl -sO https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
	mv wp-cli.phar /usr/local/bin/wp
	chmod +x /usr/local/bin/wp
fi

if test ! -x /usr/local/bin/composer
then
	echo "Downloading Composer"
	curl -s https://getcomposer.org/installer > composer-setup.php
	php composer-setup.php --install-dir=/usr/local/bin --filename=composer
	rm composer-setup.php
	chmod +x /usr/local/bin/composer
fi

if test ! -z "$DOWNLOAD_WORDPRESS_VERSION"
then
	wp core download \
		--allow-root \
		--path="/var/www/html" \
		--force \
		--version="$DOWNLOAD_WORDPRESS_VERSION" \
		--locale="${WORDPRESS_LOCALE:-en_AU}"
fi

wp core install \
	--allow-root \
	--path="/var/www/html" \
	--url="http://localhost:8000" \
	--title="Plugin Dev WordPress image" \
	--admin_user=plugin_admin \
	--admin_password=plugin_admin \
	--admin_email=machinepack-plugin@mailinator.com \

pushd /var/www/html/wp-content/plugins/machinepack
export COMPOSER_ALLOW_SUPERUSER=1
if test -f composer-dev.json
then
	COMPOSER=composer-dev.json composer install
	wp config set --allow-root WP_DEBUG true
else
	composer install
fi
popd

wp rewrite structure --allow-root '/%year%/%monthnum%/%postname%'
wp rewrite flush --allow-root
wp plugin activate --allow-root --path=/var/www/html/ machinepack

for TEST_FILE in /var/www/html/wp-content/plugins/machinepack/test/*.txt
do
	wp post create $TEST_FILE \
		--allow-root \
		--path=/var/www/html/ \
		--post_type=page \
		--post_status=publish \
		--post_title="$(basename $TEST_FILE .txt)"
done

if test ! -f /usr/local/etc/php/conf.d/errors_stderr.ini
then
	cat << EOCONFIG > /usr/local/etc/php/conf.d/errors_stderr.ini
display_errors=Off
error_log=/dev/stderr
log_errors=On
EOCONFIG

	apachectl restart
fi
