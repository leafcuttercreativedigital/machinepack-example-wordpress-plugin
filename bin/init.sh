#!/bin/bash -eux

docker-compose up -d

cat $(dirname $0)/init-docker.inc.sh | docker-compose exec -T wp bash -xc 'source /dev/stdin'
