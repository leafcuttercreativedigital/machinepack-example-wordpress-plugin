#!/bin/bash

current_version=$(git tag | sort --version-sort  | tail -1)

echo -n "Enter new version (current: ${current_version}): "
read new_version

if test -z "$new_version"
then
	echo "Version should be something like vX.Y.Z"
	exit 1
fi

plugin_file=$(dirname $0)/../plugin/machinepack.php
plugin_version=${new_version:1}
sed -i "s/^ \* Version:         .*\$/ \* Version:         ${plugin_version}/" $plugin_file
git add $plugin_file
git commit -m "Version bumped to $new_version"
git tag -f $new_version
