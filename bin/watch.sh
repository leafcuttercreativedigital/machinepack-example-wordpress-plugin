#!/bin/bash -eux

find plugin -name '*.php' \
| grep -v plugin/vendor \
| entr -c bash -c 'plugin/vendor/bin/phpcbf && plugin/vendor/bin/phpcs'
