import './main'
import App from './App'

Vue.use(MachinePack, {
	apiRoot: 'http://localhost:8000/wp-json/machinepack/1.0/'
})

new Vue({
	el: '#machinepack-wordpress-plugin-app',
	render: h => h (App)
})
