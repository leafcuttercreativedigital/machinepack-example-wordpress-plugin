import Vue from 'vue'
import MachinePack from 'machinepack-vue-ui-plugin'

window.Vue = Vue
global.Vue = Vue
window.MachinePack = MachinePack
global.MachinePack = MachinePack
