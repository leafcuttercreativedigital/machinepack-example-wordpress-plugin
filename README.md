# Machine pack WordPress plugin

This plugin exposes machine pack functionality as wordpress shortcodes and APIs.

This code base was generating using WP-CLI plugin scaffolding.

# Install

1. `composer install`
2. `git submodule update --init`

# Testing

Run `vendor/bin/phpcs` to comply with wordpress coding guidelines.

# Developing

## Plugin PHP code

Often you'll want to develop while editing core. To do this, use the provided `composer-dev.json` file, e.g.:

```
COMPOSER=composer-dev.json composer update
```

Make sure you edit that file and use this in config (check example provided). If you use
this method, you must start your docker containers with the core repo mapped correctly.
To do that, set the environment variable `MACHINEPACK_CORE` to your project path in a `.env` file, e.g.:

```
MACHINEPACK_CORE=../core
```

## Vue Front end

In `frontend`, run `npm run dev` to start a webpack based development server. For best
results while developing Vue components, use `npm link` to link the [machinepack-vue-ui-plugin](https://bitbucket.org/leafcuttercreativedigital/machinepack-vue-ui-plugin).

Once you're happy and want to install the built version into wordpress, run `npm run build`. This
will add the `machinepack-ui.js` file needed to render all UIs.

# Running the plugin

To run the plugin you can initialise a local wordpress server via `bin/init.sh`.

There is a `docker-compose` service config available, with these `.env` variables:

Variable | Example | Description |
- | -
`MACHINEPACK_CORE` | `../core` | where does your core package live on your local system
`DOWNLOAD_WORDPRESS_VERSION` | `latest`, `4.8` | what wordpress version you'd like to use (only used in `init.sh`)

# Packaging a release

**FIXME** Documentation in progress.

## Scaffolding

With that environment, you can also re-scaffold the plugin using:
```
wp scaffold plugin machinepack --path=www
```

Or by running it inside the container.

Generate a scaffold using:
```
# start a mysql server
docker run mysql -eMYSQL_ROOT_PASSWORD=root
# another terminal, install wordpress locally
mysql -h$DOCKER_CONTAINER_IP -uroot -proot -e 'create database wp;'
wp core download --path=www
wp config create --path=www --dbname=wp --dbuser=root --dbpass=root --dbhost=$DOCKER_CONTAINER_IP
wp core install --url=localhost --title=plugin-site --admin_user=plugin --admin_email=plugin@mailinator.com --path=www
# copy scaffolded plugin to this directory
rsync -av www/wp-content/plugins/machinepack/ .
```

It's good practise to regularly scaffold over the existing code base to check for
recommended plugin code standards.
