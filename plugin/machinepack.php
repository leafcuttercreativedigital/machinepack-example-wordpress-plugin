<?php
/**
 * Plugin Name:     MachinePack
 * Plugin URI:      https://leafcutter.com.au
 * Description:     Machinepack connects your WordPress site to various integrations
 * Author:          Leafcutter Team
 * Author URI:      https://leafcutter.com.au
 * Text Domain:     machinepack
 * Domain Path:     /
 * Version:         1.1.0
 *
 * @package         Machinepack
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'MACHINEPACK_CONFIG' ) ) {
	define( 'MACHINEPACK_CONFIG', __DIR__ . '/default_config.yml' );
}

if ( ! defined( 'MACHINEPACK_VERSION' ) ) {
	$plugin_data = get_file_data( __FILE__, array( 'Version' => 'Version' ), false );
	define( 'MACHINEPACK_VERSION', $plugin_data['Version'] );
}

/**
 * Import MachinePack libraries
 */
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Import actions, shortcodes, etc
 */
require_once __DIR__ . '/machinepack-action-api.php';
require_once __DIR__ . '/machinepack-action-scripts.php';
require_once __DIR__ . '/machinepack-action-login.php';
require_once __DIR__ . '/machinepack-shortcode-payment.php';
