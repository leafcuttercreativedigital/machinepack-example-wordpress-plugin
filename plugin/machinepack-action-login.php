<?php
/**
 * Example action to catch login events and send them somewhere.
 *
 * @package         Machinepack
 */

use MachinePack\Core\MachinePack as MP;

/**
 * Example hook for when a user logs in to WordPress
 */
add_action(
	'wp_login',
	function( $user_login, $user ) {
		MP::send(
			'Login.success',
			[
				'Person.givenName'  => strval( $user->first_name ),
				'Person.familyName' => strval( $user->last_name ),
			]
		);
	},
	10,
	2
);
