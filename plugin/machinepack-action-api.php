<?php
/**
 * Action for JSON API
 *
 * @package         Machinepack
 */

/**
 * Load API class
 *
 * @var MachinePack_WordPress_API
 */
require_once __DIR__ . '/class-machinepack-wordpress-api.php';

/**
 * Init hook for REST APIs
 */
add_action(
	'rest_api_init',
	function () {
		if ( ! defined( 'MACHINEPACK_CONFIG' ) ) {
			define( 'MACHINEPACK_CONFIG', __DIR__ . '/default_config.yml' );
		}
		$mp = new MachinePack_WordPress_API();
		$mp->register_routes();
	}
);
