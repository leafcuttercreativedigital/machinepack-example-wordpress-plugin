<?php
/**
 * Include MachinePack UI elements on page scripts
 *
 * @package         Machinepack
 */

add_action(
	'wp_enqueue_scripts',
	function () {
		$filename = 'machinepack-ui.js';
		wp_register_script(
			'machinepack-ui',
			plugin_dir_url( __FILE__ ) . DIRECTORY_SEPARATOR . $filename,
			[],
			sprintf( '%s-%s', MACHINEPACK_VERSION, sha1_file( __DIR__ . DIRECTORY_SEPARATOR . $filename ) ),
			true
		);
	}
);
