<?php
/**
 * Payment shortcode definition.
 *
 * @package         Machinepack
 */

use MachinePack\Core\MachinePack as MP;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;

add_shortcode(
	'machinepack_payment',
	function ( $atts ) {
		$twig   = new Twig_Environment( new Twig_Loader_Filesystem( __DIR__ ) );
		$tpl    = $twig->load( 'machinepack-shortcode-payment.twig' );
		$config = MP::config();

		$payment_keys = preg_grep( '/^payment\.request.*$/', array_keys( $config['events'] ) );

		$settings = [];
		foreach ( $payment_keys as $event ) {
			foreach ( $config['events'][ $event ] as $alias ) {
				$handler = MP::handler( $alias );
				if ( method_exists( $handler, 'getPaymentSettings' ) ) {
					$settings += (array) $handler->getPaymentSettings();
				}
			}
		}

		$data = [
			'id'       => 'machinepack-payment-' . uniqid(),
			'settings' => $settings,
			'config'   => $config,
			'api_root' => get_rest_url( null, 'machinepack/1.0/' ),
		];

		wp_enqueue_script( 'machinepack-ui' );

		return $tpl->render( $data );
	}
);
